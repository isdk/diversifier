//crypto helper class for sha1_hmac encoding

var Crypto = {};

Crypto.sha1_hmac = function (msg, key) {
    "use strict";
    var oKeyPad, iKeyPad, iPadRes, bytes, i, len;
    if (key.length > 64) {
        // keys longer than blocksize are shortened
        key = Crypto.sha1(key, true);
    }

    bytes = [];
    len = key.length;
    for (i = 0; i < 64; ++i) {
        bytes[i] = len > i ? key.charCodeAt(i) : 0x00;
    }

    oKeyPad = "";
    iKeyPad = "";

    for (i = 0; i < 64; ++i) {
        oKeyPad += String.fromCharCode(bytes[i] ^ 0x5C);
        iKeyPad += String.fromCharCode(bytes[i] ^ 0x36);
    }

    iPadRes = Crypto.sha1(iKeyPad + msg, true);

    return Crypto.sha1(oKeyPad + iPadRes);
};

Crypto.sha1 = function (msg, raw) {
    function rotate_left(n,s) {
        var t4 = ( n<<s ) | (n>>>(32-s));
        return t4;
    }

    function lsb_hex(val) {
        var str="";
        var i;
        var vh;
        var vl;

        for( i=0; i<=6; i+=2 ) {
            vh = (val>>>(i*4+4))&0x0f;
            vl = (val>>>(i*4))&0x0f;
            str += vh.toString(16) + vl.toString(16);
        }
        return str;
    }

    function cvt_hex(val, raw) {
        var str="";
        var i;
        var v;

        for( i=7; i>=0; i-- ) {
            v = (val>>>(i*4))&0x0f;
            str += raw ? String.fromCharCode(v) : v.toString(16);
        }
        return str;
    }

    var blockstart;
    var i, j;
    var W = new Array(80);
    var H0 = 0x67452301;
    var H1 = 0xEFCDAB89;
    var H2 = 0x98BADCFE;
    var H3 = 0x10325476;
    var H4 = 0xC3D2E1F0;
    var A, B, C, D, E;
    var result, rawResult;

    var msg_len = msg.length;

    var word_array = [];
    for( i=0; i<msg_len-3; i+=4 ) {
        j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |
        msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);
        word_array.push( j );
    }

    switch( msg_len % 4 ) {
        case 0:
            i = 0x080000000;
        break;
        case 1:
            i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;
        break;

        case 2:
            i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;
        break;

        case 3:
            i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8    | 0x80;
        break;
    }

    word_array.push( i );

    while( (word_array.length % 16) != 14 ) word_array.push( 0 );

    word_array.push( msg_len>>>29 );
    word_array.push( (msg_len<<3)&0x0ffffffff );

    for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {
        for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
        for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);

        A = H0;
        B = H1;
        C = H2;
        D = H3;
        E = H4;

        for( i= 0; i<=19; i++ ) {
            temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
            E = D;
            D = C;
            C = rotate_left(B,30);
            B = A;
            A = temp;
        }

        for( i=20; i<=39; i++ ) {
            temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
            E = D;
            D = C;
            C = rotate_left(B,30);
            B = A;
            A = temp;
        }

        for( i=40; i<=59; i++ ) {
            temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
            E = D;
            D = C;
            C = rotate_left(B,30);
            B = A;
            A = temp;
        }

        for( i=60; i<=79; i++ ) {
            temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
            E = D;
            D = C;
            C = rotate_left(B,30);
            B = A;
            A = temp;
        }

        H0 = (H0 + A) & 0x0ffffffff;
        H1 = (H1 + B) & 0x0ffffffff;
        H2 = (H2 + C) & 0x0ffffffff;
        H3 = (H3 + D) & 0x0ffffffff;
        H4 = (H4 + E) & 0x0ffffffff;
    }

    result = (cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4)).toLowerCase();

    if (!raw) {
        return result;
    }

    rawResult = "";
    while (result.length) {
        rawResult += String.fromCharCode(parseInt(result.substr(0, 2), 16));
        result = result.substr(2);
    }
    return rawResult;
};

function getUrlParam(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var SSOManager = {};

//SSOManager.clientId = 'UuOis8Cyvp179FQ';
SSOManager.clientId = '0oadv75yu4T3K7DK7357';
//SSOManager.clientSecret = '4Wt9Ua24upOrJ4bXPwECgJh6DOw'
SSOManager.clientSecret = 'qldU0LmnWl3jD0iXqRP7Vp0-UGr8taU5hSGDRhJp';
SSOManager.redirectUri = 'http://takeda.mydiversifier.com';
SSOManager.grantType = 'authorization_code';
//SSOManager.tokenEndpoint = 'https://login.xecurify.com/moas/rest/oauth/token';
SSOManager.tokenEndpoint = 'https://takeda.okta.com/oauth2/default/v1/token';
//SSOManager.scope = 'profile email openid offline_access';
//SSOManager.scope = 'openid+email+profile';
SSOManager.scope = 'email+openid+profile';
//SSOManager.authEndpoint = 'https://login.xecurify.com/moas/idp/openidsso?client_id=' + SSOManager.clientId + '&response_type=code&scope=' + encodeURIComponent(SSOManager.scope) + '&state=' + new Date().getTime() + '&redirect_uri=' + encodeURIComponent(SSOManager.redirectUri);
//ex: https://takeda.okta.com/oauth2/v1/authorize?client_id=0oadv75yu4T3K7DK7357&response_type=code&scope=openid+profile&state=123&redirect_uri=http://takeda.mydiversifier.com
//SSOManager.responseType = 'code';
SSOManager.responseType = 'id_token token';
SSOManager.authEndpoint = 'https://takeda.okta.com/oauth2/default/v1/authorize?client_id=' + SSOManager.clientId + '&response_type=' + SSOManager.responseType + '&scope=' + SSOManager.scope + '&state=' + new Date().getTime() + '&redirect_uri=' + encodeURIComponent(SSOManager.redirectUri) + '&nonce=' + new Date().getTime();
//SSOManager.userInfoEndpoint = 'https://login.xecurify.com/moas/rest/oauth/getuserinfo';
SSOManager.userInfoEndpoint = 'https://takeda.okta.com/oauth2/default/v1/userinfo';
SSOManager.accessTokenStorageKey = 'sso_access_token';

SSOManager.getAuthCode = function(){
    var code = getUrlParam('code');
    return code;
};

SSOManager.getLocalAccessToken = function(){
    return window.localStorage.getItem(SSOManager.accessTokenStorageKey);
};

SSOManager.setLocalAccessToken = function(accessToken){
    window.localStorage.setItem(SSOManager.accessTokenStorageKey, accessToken);
}

SSOManager.resetLocalAccessToken = function(){
    window.localStorage.removeItem(SSOManager.accessTokenStorageKey);
}

SSOManager.stripUrlParameters = function(){
    return document.location.protocol + '//' +  document.location.host +  document.location.pathname;
}

SSOManager.getAccessToken = function(code, callback){
    var codelessUrl = SSOManager.stripUrlParameters();
    var url = SSOManager.tokenEndpoint;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);

    xhr.setRequestHeader("Accept", "*/*");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            try{
                var accessTokenResp = JSON.parse(xhr.responseText);
                if(accessTokenResp == null || accessTokenResp.error != null){
                    if(accessTokenResp.error != null){
                        console.log('Getting accessToken error: ' + accessTokenResp.error);
                    }
                    if(accessTokenResp.error_description != null){
                        console.log('Getting accessToken error_description: ' + accessTokenResp.error_description);
                    }
                    SSOManager.resetLocalAccessToken();
                    window.location.href = codelessUrl;
                }
                else{
                    callback(accessTokenResp);
                }
            }
            catch(ex){
                console.log('Getting accessToken exception: ' + ex.message);
                SSOManager.resetLocalAccessToken();
                window.location.href = codelessUrl;
            }
        }
        //else{
        //    SSOManager.resetLocalAccessToken();
        //    window.location.reload();
        //}
    };

    var data = "grant_type=" + SSOManager.grantType;
    data += "&code=" + code;
    data += "&client_id=" + SSOManager.clientId;
    data += "&client_secret=" + SSOManager.clientSecret;
    data += "&redirect_uri=" + SSOManager.redirectUri;

    xhr.send(data);
};


SSOManager.getUser = function(accessToken, callback){
    var codelessUrl = SSOManager.stripUrlParameters();
    var url = SSOManager.userInfoEndpoint;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.setRequestHeader("Accept", "*/*");
    xhr.setRequestHeader("Authorization", "Bearer " + accessToken);
    
    xhr.onreadystatechange = function () {
       if (xhr.readyState === 4) {
          console.log(xhr.status);
          console.log(xhr.responseText);
          try{
              var userResp = JSON.parse(xhr.responseText);
              console.log('Get userinfo: ' + xhr.responseText);
              if(userResp == null || userResp.error != null){
                  if(userResp.error != null){
                      console.log('Getting userinfo error: ' + userResp.error);
                  }
                  if(userResp.error_description != null){
                      console.log('Getting userinfo error_description: ' + userResp.error_description);
                  }
                  
                  SSOManager.resetLocalAccessToken();
                  window.location.href = codelessUrl;
              }
              else{
                  callback(userResp);
              }
          }
          catch(ex){
              console.log('Getting userinfo exception: ' + ex.message);
              SSOManager.resetLocalAccessToken();
              window.location.href = codelessUrl;
          }
       }
       //else{
          // SSOManager.resetLocalAccessToken();
         //  window.location.href = codelessUrl;
       //}
    };
    xhr.send();
};

SSOManager.setSsoProfile = function(userInfo){
    var appId = 'custom-app-67463506-1';
    var appSecret = 'xX5ZsUPAgabdMQZ9jhdbht0JnlIi93ma';
    //var name = userInfo['firstname'] + ' ' + userInfo['lastname'];
    var name = userInfo['given_name'] + ' ' + userInfo['family_name'];
    var username = userInfo['nickname'];
    var message = btoa("{appClientId: '" + appId + "', userId:'" + username + "', profile: { email:'" + userInfo['email'] + "', billingPerson: { name: '" + name + "' } }}"); 
    var timestamp = parseInt(new Date().getTime() / 1000);
    var hmac = Crypto.sha1_hmac(message + ' ' + timestamp, appSecret);
    console.log('HMAC generated...');
    //2. set SSO profile in ecwid
    var ecwidSsoProfile = message + ' ' + hmac + ' ' + timestamp;
    try{
        Ecwid.setSsoProfile(ecwidSsoProfile);
        console.log('SSO profile set');
    }
    catch(ex){
        console.log('Error setting SSO profile: ' + ex.message);
    }
};

var ecwid_sso_profile = "";

function onEcwidJsAPILoaded(){
    console.log('onEcwidJsAPILoaded called...');
    var codelessUrl = SSOManager.stripUrlParameters();
	
    var error = getUrlParam('error');
    var errorDescr = getUrlParam('error_description');
    if((error != null && error != '') || (errorDescr != null && errorDescr != '')){
        var errorMsg = (errorDescr != null && errorDescr != '') ? errorDescr : error;
        console.log(errorMsg);
        return;
    }
    var ssoCode = SSOManager.getAuthCode();
    if(ssoCode != null && ssoCode != '' && ssoCode != 'undefined'){
        //first check if we have logging in
        console.log('Auth code: ' + ssoCode);
        console.log('Fetching access token...');
        //case 1: user is authorized, get access token
        SSOManager.getAccessToken(ssoCode, function(accessTokenResp){
            var accessToken = accessTokenResp['access_token'];
            console.log('Access token fetched: ' + accessToken);
            if(accessToken != null && accessToken != ''){
                SSOManager.getUser(accessToken, function(userInfo){
                    if(userInfo != null){
                        console.log('User fetched: ' + userInfo['email']);
                        SSOManager.setSsoProfile(userInfo);
                        SSOManager.setLocalAccessToken(accessToken);
                    }
                    else{
                        console.log('User is not fetched!');
                    }
                });
            }
            else{
                SSOManager.resetLocalAccessToken();
                window.location.href = codelessUrl;
            }
        });
    }
    else{
        //check for access_token for implicit flow
        var accessTokenFromUrl = getUrlParam('access_token');
        console.log('Fetching access token from url: ' + accessTokenFromUrl);
        if(accessTokenFromUrl != null && accessTokenFromUrl != '' && accessTokenFromUrl != 'undefined'){
            console.log('Access token is fetched from url');
            SSOManager.getUser(accessTokenFromUrl, function(userInfo){
                if(userInfo != null){
                    console.log('User prefetched: ' + JSON.stringify(userInfo, null, 4));
                    SSOManager.setSsoProfile(userInfo);
                }
                else{
                    console.log('User is not fetched!');
                }
            });
        }
        else{
            console.log('Access token is NOT fetched from url');
            //case 2: user is not logging in, try to get accessToken
            var storedAccessToken = SSOManager.getLocalAccessToken();
            if(storedAccessToken != null && storedAccessToken != '' && storedAccessToken != 'undefined'){
                SSOManager.getUser(storedAccessToken, function(userInfo){
                    if(userInfo != null){
                        console.log('User prefetched: ' + JSON.stringify(userInfo, null, 4));
                        SSOManager.setSsoProfile(userInfo);
                    }
                    else{
                        console.log('User is not fetched!');
                    }
                });
            }
            else{
                //case 3: user is not authorized, add SSO login URL
	            var ssoUrl = SSOManager.authEndpoint;
                console.log('No auth code, adding SSO signing URL: ' + ssoUrl);
	            try{
	               //Ecwid.setSignInUrls({
                        //signInUrl: ssoUrl
                    //});
                    window.location.href = ssoUrl;
                    console.log('SSO URL is set');
                }
                catch(ex){
                    console.log('Error setting SSO URL: ' + ex.message);
                }
            }
        }
    }
	
	
	//Case 1: authorized session, autthorized user is redirected back from IdP
	//TODO: read SAML response
	//1. build hmac of authorized user
	//var message = btoa("{appClientId: '" + clientId + "', userId:'kirill.rebrov@isdk.pro', profile: { email:'kirill.rebrov@isdk.pro', billingPerson: { name: 'John Doe' } }}"); 
	//var timestamp = parseInt(new Date().getTime() / 1000);
	//var hmac = Crypto.sha1_hmac(message + ' ' + timestamp, clientSecret);
    //console.log('HMAC generated...');
	//2. set SSO profile in ecwid
	//var ecwidSsoProfile = message + ' ' + hmac + ' ' + timestamp;
	//Ecwid.setSsoProfile(ecwidSsoProfile);

	
}

Ecwid.OnAPILoaded.add(onEcwidJsAPILoaded);
